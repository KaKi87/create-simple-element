import { createParser } from 'https://cdn.jsdelivr.net/npm/css-selector-parser/+esm';

import createSimpleElementCreator from './createSimpleElementCreator.js';

export default createSimpleElementCreator({
    createParser,
    document: window.document
});