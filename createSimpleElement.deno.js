import { createParser } from 'npm:css-selector-parser';
import { DOMParser } from 'https://raw.githubusercontent.com/b-fuze/deno-dom/master/deno-dom-wasm.ts';

import createSimpleElementCreator from './createSimpleElementCreator.js';

export default createSimpleElementCreator({
    createParser,
    document: new DOMParser().parseFromString('', 'text/html')
});