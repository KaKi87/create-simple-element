# `create-simple-element`

Create simple HTML elements using CSS selectors.

## Import

### From browser

```js
import createSimpleElement from 'https://cdn.jsdelivr.net/npm/create-simple-element/createSimpleElement.js';
```

### From Deno

Uses [`deno-dom`](https://github.com/b-fuze/deno-dom).
To use a different HTML implementation, see [Custom usage](#custom).

```js
import createSimpleElement from 'https://cdn.jsdelivr.net/npm/create-simple-element/createSimpleElement.deno.js';
```

### From Node

Requires [`jsdom`](https://github.com/jsdom/jsdom) : `yarn add jsdom`.
To use a different HTML implementation, see [Custom usage](#custom).

```js
import createSimpleElement from 'create-simple-element';
```

## Usage

### Default

```js
console.log(createSimpleElement('input#foo.bar[type="password"]:required').outerHTML);
// -> <input id="foo" class="bar" type="password" required="">

console.log(createSimpleElement('ul.foo > li.foo__bar + li.foo__bar.foo__bar--baz').outerHTML);
// -> <ul class="foo"><li class="foo__bar"></li><li class="foo__bar foo__bar--baz"></li></ul>
```

![](./screenshot.png)

### Custom

Here's an example using [`happy-dom`](https://github.com/capricorn86/happy-dom)
as HTML implementation on Node instead of the default one.

```js
import { createParser } from 'css-selector-parser';
import { Window } from 'happy-dom';

import createSimpleElementCreator from './createSimpleElementCreator.js';

const createSimpleElement = createSimpleElementCreator({
    createParser,
    document: (new Window()).document
});

console.log(createSimpleElement('input#foo.bar[type="password"]:required').outerHTML);
// -> <input id="foo" class="bar" type="password" required="">
```

### Supported items

- ID (`#foo`)
- Class (`.foo`)
- Attribute (`[foo="bar"]`)
- Pseudo-classes (that actually create attributes) :
  - `:checked` (short for `[checked=""]`)
  - `:disabled` (short for `[disabled=""]`)
  - `:required` (short for `[required=""]`)
- Combinators :
  - Child (`>`)
  - Next sibling (`+`)

## Emmet

This module only supports the CSS syntax,
e.g. it doesn't support grouping & multiplying like [`emmet`](https://github.com/emmetio/emmet) does.

For this purpose, use [`emel`](https://github.com/UziTech/emel) instead :

```js
import emel from 'https://cdn.jsdelivr.net/npm/emel/+esm';
console.log(emel('ul.foo>li.foo__bar*2', { returnSingleChild: true }).outerHTML);
// -> <ul class="foo"><li class="foo__bar"></li><li class="foo__bar"></li></ul>
```