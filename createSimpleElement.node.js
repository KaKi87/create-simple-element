import { createParser } from 'css-selector-parser';
import { JSDOM } from 'jsdom';

import createSimpleElementCreator from './createSimpleElementCreator.js';

export default createSimpleElementCreator({
    createParser,
    document: (new JSDOM()).window.document
});