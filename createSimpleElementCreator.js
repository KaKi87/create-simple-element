/**
 * Create a simple HTML element from a CSS selector.
 * @typedef simpleElementCreator
 * @param {string} selector - CSS selector
 * @returns {HTMLElement} - HTML element
 */

/**
 * @param {function} createParser - CSS selector parser creator
 * @param {Document} document - HTML document
 * @returns {simpleElementCreator} - Simple element creator
 */
export default ({
    createParser,
    document
}) => {
    const
        /** @type {import('css-selector-parser').Parser} */
        parse = createParser(),
        createElement = (rule, parentElement) => {
            const
                { items } = rule,
                [firstItem] = items;
            if(firstItem.type !== 'TagName')
                throw new Error('Selector must start with tag name.');
            const element = document.createElement(firstItem.name);
            if(parentElement)
                parentElement.appendChild(element);
            for(let itemIndex = 1; itemIndex < items.length; itemIndex++){
                const item = items[itemIndex];
                switch(item.type){
                    case 'Id': {
                        element.setAttribute('id', item.name);
                        break;
                    }
                    case 'ClassName': {
                        element.classList.add(item.name);
                        break;
                    }
                    case 'Attribute': {
                        element.setAttribute(item.name, item.value.value);
                        break;
                    }
                    case 'PseudoClass': {
                        switch(item.name){
                            case 'checked':
                            case 'disabled':
                            case 'required': {
                                element.setAttribute(item.name, '');
                                break;
                            }
                            default: throw new Error(`Unsupported "${item.name}" pseudo-class.`);
                        }
                        break;
                    }
                    default: throw new Error(`Unsupported "${item.type}" type.`);
                }
            }
            if(rule.nestedRule) switch(rule.nestedRule.combinator){
                case '>': {
                    createElement(rule.nestedRule, element);
                    break;
                }
                case '+': {
                    if(parentElement)
                        createElement(rule.nestedRule, parentElement);
                    else
                        throw new Error('Selector must only contain one root element.');
                    break;
                }
                default: throw new Error(`Unsupported "${rule.nestedRule.combinator}" combinator.`);
            }
            return element;
        };
    /** @type {simpleElementCreator} */
    return selector => {
        const parsedSelector = parse(selector);
        if(
            parsedSelector.type !== 'Selector'
            ||
            parsedSelector.rules.length !== 1
            ||
            parsedSelector.rules[0].type !== 'Rule'
        ) throw new Error('Selector must only contain one root element.');
        return createElement(parsedSelector.rules[0]);
    };
};